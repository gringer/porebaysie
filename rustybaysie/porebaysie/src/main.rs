use std::io;
use std::io::SeekFrom;
use std::io::prelude::*;
use std::fs::File;
// for pod5 reading
use arrow2::ipc::read_stream;
//use arrow::record_batch::RecordBatch;
use arrow2::error::ArrowError;



//use std::ffi::CString;

// OpenAI suggestion
//use std::io::{Read, Cursor};

// Read an 8-byte integer from the input stream
//fn read_i64(input_stream: &mut dyn Read) -> i64 {
//  let mut buffer = [0u8; 8];
//  input_stream.read_exact(&mut buffer).unwrap();
//  i64::from_le_bytes(buffer)
//}

// Read a variable-length string from the input stream
//fn read_string(input_stream: &mut dyn Read) -> String {
//  let str_len = read_i64(input_stream) as usize;
//  let mut buffer = vec![0u8; str_len];
//  input_stream.read_exact(&mut buffer).unwrap();
//  String::from_utf8(buffer).unwrap()
//}

// Read a field from the input stream
//fn read_field(input_stream: &mut dyn Read) -> (String, String) {
//  let field_name = read_string(input_stream);
//  let field_type = read_string(input_stream);
//  (field_name, field_type)
//}

//fn parse_first_few_fields(input_stream: &mut dyn Read) -> Result<(), ArrowError> {
  // Read the magic number and version from the Arrow file header
  //let magic_number = read_i64(input_stream);
  //let version = read_i32(input_stream);

  // Check that the magic number and version are correct
  //if magic_number != 0x41_52_52_4F_57_31_00_00 && version != 0 {
  //  return Err(ArrowError::InvalidHeader);
  //}

  // Read the metadata from the Arrow file header
  //let metadata_length = read_i32(input_stream) as usize;
  //let mut metadata_buffer = vec![0u8; metadata_length];
  //input_stream.read_exact(&mut metadata_buffer).unwrap();
  //let metadata = String::from_utf8(metadata_buffer).unwrap();

  // Parse the metadata JSON to get the schema of the data
  //let metadata_json: Value = serde_json::from_str(&metadata)?;
  //let schema_json = metadata_json["schema"]["fields"].as_array().unwrap();

  // Iterate through the first few fields in the schema
  //for i in 0..schema_json.len().min(5) {
  //  let field = &schema_json[i];
  //  let field_name = field["name"].as_str().unwrap();
  //  let field_type = field["type"].as_str().unwrap();

    // Print the field name and type
  //  println!("{}: {}", field_name, field_type);
  //}

  //Ok(())
//}

// Or...

const POD5_SIGNATURE: [u8; 8] = [139, 80, 79, 68, 13, 10, 26, 10]; // [\139, "POD5", CR, LF, EOF, LF]
const ARROW_SIGNATURE: [u8; 8] = [65, 82, 82, 79, 87, 49, 0, 0]; // [ARROW1, \0, \0]
const CONTINUATION_SIGNATURE: u32 = 0xFFFFFFFF; // A 32-bit continuation indicator. The value 0xFFFFFFFF indicates a valid message.
// https://arrow.apache.org/docs/java/reference/constant-values.html#org.apache.arrow.flatbuf.Endianness.Little
const ENDIANNESS_BIG: u8 = 1;
const ENDIANNESS_LITTLE: u8 = 0;
// https://github.com/google/flatbuffers/blob/master/java/src/main/java/com/google/flatbuffers/Constants.java
const SIZEOF_BYTE: u8 = 1;
const SIZEOF_SHORT: u8 = 2;
const SIZEOF_INT: u8 = 4;
const SIZEOF_FLOAT: u8 = 4;
const SIZEOF_LONG: u8 = 8;
const SIZEOF_DOUBLE: u8 = 8;
// https://github.com/apache/arrow/blob/cee703ba84883444c9bad940531f49d1a52e156c/java/format/src/main/java/org/apache/arrow/flatbuf/Message.java#L28
const ARROW_FLATC_VERSION: &str = "1_12_0"; 

fn pad8(val: u32) -> u32 {
    0 + (val + (8 - val % 8) % 8)
}

// POD5 format specification:
// https://github.com/nanoporetech/pod5-file-format/blob/master/docs/SPECIFICATION.md#layout
// https://github.com/nanoporetech/pod5-file-format/blob/master/docs/tables/reads.toml
// https://github.com/nanoporetech/pod5-file-format/blob/master/docs/tables/signal.toml
// References Apache Arrow IPC File Format / Feather:
// https://arrow.apache.org/docs/format/Columnar.html#encapsulated-message-format
// https://arrow.apache.org/docs/format/Columnar.html#serialization-and-interprocess-communication-ipc
// https://github.com/apache/arrow/blob/master/format/Schema.fbs
// Java Code:
// https://github.com/apache/arrow/tree/master/java/format/src/main/java/org/apache/arrow/flatbuf
// https://github.com/google/flatbuffers/tree/master/java/src/main/java/com/google/flatbuffers
// https://github.com/google/flatbuffers/blob/master/java/src/main/java/com/google/flatbuffers/FlatBufferBuilder.java
// https://github.com/apache/arrow/blob/master/java/vector/src/main/java/org/apache/arrow/vector/ipc/message/MessageSerializer.java
// Rust Arrow:
// https://docs.rs/arrow2/latest/arrow2/

fn parse_metadata(read_data: Vec<u8>) {
    // https://arrow.apache.org/docs/format/CDataInterface.html?highlight=metadata#c.ArrowSchema.metadata
    // https://github.com/apache/arrow/blob/master/java/format/src/main/java/org/apache/arrow/flatbuf/Message.java#L59
    // https://github.com/apache/arrow/tree/master/java/vector/src/main/java/org/apache/arrow/vector/ipc
    let mut current_pos = 0;
    let mut val = read_data[current_pos]; current_pos += 1;
    println!("    Version: {}", val);
    val = read_data[current_pos]; current_pos += 1;
    println!("    Endianness: {}", val);
}

// Created with help by ChatGPT
fn read_arrow_chunk(mut f: &File, mut file_offset:u64) -> u64 {
    // https://arrow.apache.org/docs/format/Columnar.html#encapsulated-message-format
    let mut id = [0u8; 8];
    f.seek(SeekFrom::Start(file_offset)).expect("Unable to seek to next Arrow chunk");
    let mut size = f.read(&mut id[..]).expect("Unable to read Arrow chunk header");
    file_offset += 8;
    if size < 8 {
	panic!("Failed type check: couldn't read enough bytes");
    }
    if id == ARROW_SIGNATURE {
        println!("Found Arrow signature at {:#x}", file_offset);
        // inserted
        f.seek(SeekFrom::Start(file_offset-8)).expect("Unable to seek to next Arrow chunk");
        let mut bytes = Vec::new();
        f.take(f.metadata().unwrap().len() - 56).read_to_end(&mut bytes).unwrap();
        let (record_batch, _) = read_message(&bytes, None).unwrap();
        for j in 0..record_batch.num_columns().min(5) {
            let field = record_batch.column(j).as_ref();
            // Print the field name and type
            println!("Field {}: {:?}", j, field);
        }
        f.seek(SeekFrom::Start(file_offset)).expect("Unable to seek to next Arrow chunk");
        // ^^ inserted
        let mut read_data: Vec<u8> = Vec::new();
        let mut size_to_read = 4;
        size = f.take((size_to_read).try_into().unwrap()).
            read_to_end(&mut read_data).unwrap();
        file_offset += size as u64;
        let val = u32::from_le_bytes(read_data[0..4].try_into().unwrap());
        if val == CONTINUATION_SIGNATURE {
            println!("  Seen a continuation signature");
        }
        read_data.clear();
        size_to_read = 4;
        size = f.take((size_to_read).try_into().unwrap()).
            read_to_end(&mut read_data).unwrap();
        file_offset += size as u64;
        let metadata_size = u32::from_le_bytes(read_data[0..4].try_into().unwrap());
        println!("  metadata size: {} [{:#x}; {:#x} padded]", metadata_size, metadata_size, pad8(metadata_size));
        read_data.clear();
        size_to_read = metadata_size;
        size = f.take((size_to_read).try_into().unwrap()).
            read_to_end(&mut read_data).unwrap();
        parse_metadata(read_data);
        file_offset += size as u64;
        file_offset = pad8(file_offset.try_into().unwrap()) as u64;
        f.seek(SeekFrom::Start(file_offset)).expect("Unable to seek to message body");
        println!("  current offset: {} [{:#x}]", file_offset, file_offset);
    }
    file_offset
}

fn main() -> io::Result<()> {
    let filename = "../../good_guppy_3.6.0_LAST_gci_vs_Nb_mtDNA.pod5";
    println!("Loading from file: {}", filename);
    let mut f = &File::open(filename)?;
    let f_len = f.metadata().unwrap().len();
    let mut id = [0u8; 8];
    let mut size = f.read(&mut id[..])?;
    if size < 8 {
	panic!("Failed type check: couldn't read enough bytes");
    }
    if id == POD5_SIGNATURE {
	println!("Looks like a POD5 file");
    } else {
	panic!(concat!("Failed signature check; could not find POD5 file ",
		       "format signature at byte offset 0"));
    }
    let mut section_marker = [0u8; 16];
    size = f.read(&mut section_marker[..])?;
    if size < 16 {
	panic!("Failed section marker read: couldn't read enough bytes");
    }
    print!("Section marker signature: ");
    for ds_pos in 0 .. 16 {
        print!("{:x} ", section_marker[ds_pos as usize]);
    }
    println!("");
    f.seek(SeekFrom::Start(f_len - 8)).expect("Unable to seek to file end");
    size = f.read(&mut id[..])?;
    if size < 8 {
	panic!("Failed type check: couldn't read enough bytes");
    }
    if id == POD5_SIGNATURE {
	println!("Looks like a *complete* POD5 file");
    } else {
	println!("Failed footer signature check; POD5 file is probably not complete");
    }
    f.seek(SeekFrom::Start(f_len - 24)).expect("Unable to seek to file end");
    let mut test_sec_marker = [0u8; 16];
    size = f.read(&mut test_sec_marker[..])?;
    if size < 16 {
	panic!("Failed section marker read: couldn't read enough bytes");
    }
    print!("Footer marker signature:  ");
    for ds_pos in 0 .. 16 {
        print!("{:x} ", test_sec_marker[ds_pos]);
    }
    if test_sec_marker == section_marker {
        println!("[match]");
    } else {
        println!("[no match]");
	println!("Failed footer section marker check; POD5 file is probably not complete");
    }
    f.seek(SeekFrom::Start(24)).expect("Unable to seek to first record");
    let mut file_pos:u64 = 24;
    while file_pos < (f_len - 24) {
        file_pos = read_arrow_chunk(f, file_pos);
    }
    println!("Reached end of file");
    Ok(())
}
