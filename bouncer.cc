/* bouncer -- Brisk Open-pore Utility for Naming ChimEric Reads

   Bouncer is a nanopore raw-read filter to indentify in-silico chimerism,
   and is part of the PoreBaysie nanopore toolkit.

   Copyright (C) 2019 David Eccles (gringer) <bioinformatics@gringene.org>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <cmath>
#include <H5Cpp.h>  // HDF5 C++ utility header file

using std::setprecision;
using std::cout;
using std::cerr;
using std::endl;
using std::abs;
using std::signbit;

using namespace H5;

// https://stackoverflow.com/a/39487448/3389895
template <typename T = float, typename C>
inline const T median(const C &v){
  if(v.empty()){
    return(0.0);
  }
  std::vector<T> tv(std::begin(v), std::end(v));
  size_t n = tv.size() / 2;
  std::nth_element(tv.begin(), tv.begin() + n, tv.end());
  if(tv.size() % 2){
    return(tv[n]);
  } else {
    // even sized vector -> average the two middle values
    auto max_it = std::max_element(tv.begin(), tv.begin() + n);
    return((*max_it + tv[n]) / 2.0);
  }
}

// Trimmed mean - (mean of values that are smaller than a given threshold)
template <typename T = float, typename C>
inline const T mean(const C &v, uint16_t threshold = 1000){
  if(v.empty()){
    return(0.0);
  }
  std::vector<T> tv(std::begin(v), std::end(v));
  float total = 0;
  size_t trimCount = 0;
  for(size_t i=0; i < v.size(); i++){
    if(abs(v[i]) < threshold){
      total += v[i];
    } else {
      trimCount++;
    }
  }
  return(total / (v.size() - trimCount));
}


template <typename T = float>
inline const float mad(std::vector<T> &v, float med){
  std::vector<float> absDevs;
  absDevs.resize(v.size());
  for(size_t i=0; i < v.size(); i++){
    absDevs[i] = abs(v[i] - med);
  }
  return(median(absDevs));
}

void usage(){
  cerr << endl;
  cerr << "usage: ./bouncer " << "<multi fast5 file>" << endl << endl;
}

int main (int argc, char* argv[]){
  if(argc <= 1){
    cerr << "Error: no file specified on the command line" << endl;
    usage();
    return 1; // error state; unsuccessfully terminated
  }
  if(argc > 2){
    cerr << "Warning: more than one argument specified on the command line" << endl;
  }
  std::string fName = argv[1];
  const H5std_string FILE_NAME(fName);
  const H5std_string GROUP_NAME("/");
  const H5std_string RAWSIG_LOC("Raw/Signal");
  const H5std_string RAWID_LOC("Raw/channel_id");
  H5File file(FILE_NAME, H5F_ACC_RDONLY);
  // Inspect root group
  Group group = file.openGroup(GROUP_NAME);
  size_t nObjs = group.getNumObjs();
  for(size_t gPos = 0; gPos < nObjs; gPos++){
    // Get name based on position, then reference by that name
    // [There is probably a direct-number way to do this]
    H5std_string iName = group.getObjnameByIdx(gPos);
    Group iGroup;
    DataSet iSignal;
    hsize_t dsMS;
    iGroup = group.openGroup(iName);
    iSignal = iGroup.openDataSet(RAWSIG_LOC);
    dsMS = iSignal.getInMemDataSize() / 2;
    //cerr << " Signal Data Storage Size: " << dsMS << endl;
    std::vector<uint16_t> dsBuf;
    dsBuf.resize(dsMS);
    iSignal.read(&dsBuf[0], PredType::STD_I16LE);
    // calculate signal mean
    uint16_t dmean = mean(dsBuf);
    size_t maxOpenCount = 0;
    size_t currentOpenCount = 0;
    size_t endPos = 0;
    // If mean is too high, normalise to make 350 the mean
    bool adjustedMean = (dmean > 360);
    int16_t offset = (adjustedMean) ? (350 - dmean): 0;
    // define cutoff threshold
    uint16_t threshold = 600;
    // exclude initial pore stall state (ignoring short blips)
    size_t blipLength = 0;
    size_t startPos = 0;
    for(size_t i=50; i < dsMS; i++){
        if((dsBuf[i] + offset) > 480){
          blipLength++;
          startPos = i;
          if(blipLength > 3){
            break;
          }
        } else {
          blipLength = 0;
        }
    }
    // high-current header sequence is about 400-600 samples,
    // so start a bit further than that
    startPos = startPos + 1000;
    for(size_t i=startPos; i < (dsMS-400); i++){
      if((dsBuf[i] + offset) > threshold){
        currentOpenCount++;
      } else if(currentOpenCount > 0) {
        if(maxOpenCount < currentOpenCount){
          maxOpenCount = currentOpenCount;
          endPos = i;
        }
        currentOpenCount = 0;
      }
    }
    if(maxOpenCount > 5){
      // trim off initial "read_" text
      std::string gName = iName.substr(5);
      cout << gName << ","
           << dmean << ","
           << maxOpenCount << ","
           << endPos << endl;
    }
  }
  file.close();
  return 0;  // successfully terminated
}
