## PoreBaysie: multi-pass nanopore basecaller using bayesian probability

![PoreBaysie](porebaysie_logo_small.png "PoreBaysie logo")

## Summary / General Approach

Note: I'm working on this project intermittently in my spare time, usually while
I wait for other programs to finish what they're doing. Feel free to fork, clone,
comment, or otherwise experiment with this code.

* Pass 1 - Signal features: open pore, homopolymer regions, base calling speed, base transitions
* Pass 2 - Adapters, barcodes, etc (in signal space)
* Pass 3 - Base calls (via bayesian probabilities)
* Pass 4 - Work out modifications (where base call has low probability)

## Current status - preliminary exploration

* Read in a multi-read HDF5 file and output the signal values.
* Calculate median and MAD for scaling
* Identify and remove signal spikes (5 passes)
* Smooth de-spiked signal (running mean, 9 samples)
* Output SVG of scaled signal values (10 samples per horizontal pixel)

### Pass 1

This will be fairly standard hunt-and-peck stuff, by inspecting the raw signal
of a few reads, and trying to find interesting regions.

The general model should assume a single-stranded string of potentially-overlapping
nucleotides with a semi-rigid sugar-phosphate backbone:

    10  P B
     9  S/ B
     8  P /     Strand movement
     7  S/ B           |
     6  P /            |
     5  S/            \|/
     4  P B
     3  S/ B
     2  P /
     1  S/

If this is the case, then pyrimidine-pyrimidine dimers should have less overlap
noise, and be more likely to have distinguishable bases, compared to purine-purine
dimers... and polyA/polyC would be the hardest to resolve to single bases.

### Pass 2

This will use pre-identified (and well-characterised) signal images to enable
signal-level identification of adapter sequences and barcodes.

Ideally, the matching would be done by time-independent pattern matching (e.g. 
splines / wavelets), because there is substantial time-domain warping that goes
on in the signal.

### Pass 3

This pass should estimate instantaneous base transition rate (i.e. bases per second), in an
ideal case identifying base transition points

### Pass 4

This will be broken into two steps:
  1) model -- generate models for mapping 100-ish sample signals to a single base
     [e.g. 50 samples prior, 50 samples afterwards]
  2) call -- use models to determine most likely bases

### Pass 5

This will identify regions of low probability, and determine the likelihood
(and eventually nature) of the modification.

## Installing

PoreBaysie has one dependency: libhdf5 C++ libraries ('apt-get install libhdf5-dev' on Debian).

Assuming dependencies are installed, PoreBaysie can be run as follows:

1. Clone the gitlab repository: ``git clone https://gitlab.com/gringer/porebaysie``
2. Change into the porebaysie directory: ``cd porebaysie``
3. Run make to compile and test the program: ``make``

Subsequent runs can be carried out by running ``./porebaysie``

## Image

The PoreBaysie image is created from public domain clipart and Protein Data
Bank sequence files, together with an Open Font License font. PDB Files were
converted to PNG using the web interface, thresholded using GIMP, then
traced and combined using Inkscape:

* https://openclipart.org/detail/298203/doublestranded-dna-sequence
* https://openclipart.org/detail/117517/potty-baby-silhouette
* http://www.rcsb.org/structure/1PJR
* http://www.rcsb.org/structure/1UUN
* https://fontlibrary.org/en/font/linux-biolinum
* https://www.gimp.org
* https://inkscape.org
