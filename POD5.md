POD5 Format
===

Included in this repository [here](good_guppy_3.6.0_LAST_gci_vs_Nb_mtDNA.pod5) is a POD5 file containing three *Nippostrongylus brasiliensis* mitochondrial DNA sequences that have good base calls when called using guppy v3.6.0 and the high-accuracy model (see [here](good_guppy_3.6.0_LAST_gci_vs_Nb_mtDNA.fastq)). Gap-compressed identities (GCIs) are 98.68%, 98.16%, and 97.92% for the three sequences. An updated called version of this file (called using guppy v6.3.2 and the super-accuracy model) is also included [here](good_guppy_3.6.0_LAST_gci_vs_Nb_mtDNA_called_6.3.2.fastq), with GCIs of 98.45%, 98.69%, and 98.50% respectively. The reference genome is included [here](circ-Nb-ec3-mtDNA.fasta).

This document will eventually be populated with a fully-annotated representation of the pod5 file (excluding signal data). Hopefully this will help someone else out in the future who does not want to navigate the labyrinthine specifications for the Apache Arrow IPC / Feather file formats. This will attempt to follow the annotation convention of the ONT [POD5 specification](https://github.com/nanoporetech/pod5-file-format/blob/master/docs/SPECIFICATION.md#combined-file-layout), but include the Arrow information as well:

```
<signature "\213POD\r\n\032\n">
<section marker (0x0008): 16 bytes [d9 94 9a b3 d7 cb 45 35  ac fe bf 62 69 d0 87 e4]>
<embedded file 1 (padded to 8-byte boundary, found at 0x18)>
  <Arrow magic string: "ARROW1\000\000">
  <Arrow continuation signature (0xFFFFFFFF) at 0x20>
  <Chunk length: 0x000002c0> [i.e. next chunk will start at 0x28 + 2c0]
  <Arrow continuation signature (0xFFFFFFFF) at 0x2e8>
  <Chunk length: 0x000000f8> [i.e. next chunk will start at 0x2f0 + f8]
  <[no continuation signature found at 0x3e8, so file/record is complete]>
  <...>
<section marker (0x8400): 16 bytes [d9 94 9a b3 d7 cb 45 35  ac fe bf 62 69 d0 87 e4]>
<embedded file 2 (padded to 8-byte boundary, found at 0x8410)>
  <Arrow magic: "ARROW1\000\000">
  <Arrow continuation signature (0xFFFFFFFF)>
  <Metadata length: 0x00000af8>
  <...>
<section marker (0xaec0): 16 bytes [d9 94 9a b3 d7 cb 45 35  ac fe bf 62 69 d0 87 e4]>
<footer (padded to 8-byte boundary, found at 0xaed0 = [file size - footer length - 8])>
  <footer magic: "FOOTER\000\000">
    <Footer, formatted as a Google FlatBuffer>
      File Identifier: 4800f0e0-e07d-49c6-8b3d-df36a69d13e7
      Software: Python API
      Pod5 Version: b'0.0.23'
      Contents Length: 2
        Content[0]: offset=24, length=38962810, format=0, type=SignalTable
        Content[1]: offset=38962856, length=223514, format=0, type=ReadsTable
  <footer length: 8 bytes little-endian signed integer; including padding, but excluding magic [192]>
<section marker: 16 bytes [d9 94 9a b3 d7 cb 45 35  ac fe bf 62 69 d0 87 e4]>
<signature "\213POD\r\n\032\n">
```

# Additional Useful Notes

Arrow files can be created in serial chunks. This seems to be the format:

* The file begins with the magic string "ARROW1\0\0" (8 bytes).
* After the magic string, there is a continuation signature (4 bytes), which is always set to 0xFFFFFFFF.
* After the continuation signature, there is the length of the chunk (4 bytes), not including the continuation signature or chunk length bytes.
* Then there are a series of chunks, each preceded by a continuation signature (4 bytes), and the length of the chunk (4 bytes).
* If a continuation marker is not found, or the end of the file is reached, then the file is finished.
