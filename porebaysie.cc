/* PoreBaysie -- Multi-pass nanopore basecaller using bayesian probability
   [currently an hdf5 reader demo to extract, smooth and display raw signal]
   Copyright (C) 2019 David Eccles (gringer) <bioinformatics@gringene.org>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <cmath>
#include <H5Cpp.h>  // HDF5 C++ utility header file

#include "porebaysie.h"

using std::setprecision;
using std::cout;
using std::cerr;
using std::endl;
using std::abs;
using std::signbit;

using namespace H5;

// https://stackoverflow.com/a/39487448/3389895
template <typename T = float, typename C>
inline const T median(const C &v){
  if(v.empty()){
    return(0.0);
  }
  std::vector<T> tv(std::begin(v), std::end(v));
  size_t n = tv.size() / 2;
  std::nth_element(tv.begin(), tv.begin() + n, tv.end());
  if(tv.size() % 2){
    return(tv[n]);
  } else {
    // even sized vector -> average the two middle values
    auto max_it = std::max_element(tv.begin(), tv.begin() + n);
    return((*max_it + tv[n]) / 2.0);
  }
}

template <typename T>
float porebaysie::mad(std::vector<T> &v, float med){
  std::vector<float> absDevs;
  absDevs.resize(v.size());
  for(size_t i=0; i < v.size(); i++){
    absDevs[i] = abs(v[i] - med);
  }
  return(median(absDevs));
}

template <typename T>
std::vector<T> porebaysie::runningMean(std::vector<T> &v,
                                           size_t windowSize){
  // Calculate running mean, centred on the current location
  std::vector<T> runMean;
  runMean.resize(v.size());
  if(v.size() == 0){
    return(runMean);
  }
  size_t lastStart = 0;
  size_t lastEnd = 0;
  size_t startDist = 0;
  size_t endDist = v.size()-1;
  float currentSum = v[0];
  if(windowSize % 2 == 0){ // ensure window size is odd
    windowSize++;
  }
  size_t maxWindowOffset = windowSize >> 1;
  for(size_t i=0, startDist=0, endDist=(v.size()-1);
      i < v.size(); i++, startDist++, endDist--){
    size_t windowOffset = (startDist < endDist ? startDist : endDist);
    if(windowOffset > maxWindowOffset){
      windowOffset = maxWindowOffset;
    }
    size_t currentStart = i - windowOffset;
    size_t currentEnd = i + windowOffset;
    while(lastStart < currentStart){
      // remove any numbers that have fallen off the left window edge
      currentSum -= v[lastStart++]; // increment *after* evaluation
    }
    while(lastEnd < currentEnd){
      // add any numbers that have appeared at the right window edge
      currentSum += v[++lastEnd]; // increment *before* evaluation
    }
    runMean[i] = currentSum / ((windowOffset << 1) + 1);
  }
  return(runMean);
}

template <typename T>
std::vector<T> porebaysie::absDelta(std::vector<T> &v1,
                                        std::vector<T> &v2){
  // Calculate absolute delta between two vectors
  std::vector<float> aDelta;
  aDelta.resize(v1.size());
  if(v1.size() == 0){
    return(aDelta);
  }
  for(size_t i=0; i < v1.size(); i++){
    aDelta[i] = abs(v1[i] - v2[i]);
  }
  return(aDelta);
}

template <typename T>
std::vector<T> porebaysie::absDelta(std::vector<T> &v1, float a2){
  // Calculate absolute delta between two vectors
  std::vector<float> aDelta;
  aDelta.resize(v1.size());
  if(v1.size() == 0){
    return(aDelta);
  }
  for(size_t i=0; i < v1.size(); i++){
    aDelta[i] = abs(v1[i] - a2);
  }
  return(aDelta);
}


int main (void){
  const H5std_string FILE_NAME("Ubb_10reads.fast5");
  const H5std_string GROUP_NAME("/");
  const H5std_string RAWSIG_LOC("Raw/Signal");
  const H5std_string RAWID_LOC("Raw/channel_id");
  cerr << "Oh, Pore Baysie!" << endl;
  H5File file(FILE_NAME, H5F_ACC_RDONLY);
  // Inspect root group
  cerr << "Number of root objects: " << file.getObjCount() << endl;
  Group group = file.openGroup(GROUP_NAME);
  cerr << " Number of attributes in group: " << group.getNumAttrs() << endl;
  cerr << " Number of objects in group: " << group.getNumObjs() << endl;
  H5std_string iName = group.getObjnameByIdx(0);
  Group iGroup;
  DataSet iSignal;
  hsize_t dsMS;
  // Inspect first sequence object
  cerr << "First object: " << iName << endl;
  iGroup = group.openGroup(iName);
  iSignal = iGroup.openDataSet(RAWSIG_LOC);
  dsMS = iSignal.getInMemDataSize() / 2;
  cerr << " Signal Data Storage Size: " << dsMS << endl;
  std::vector<uint16_t> dsBuf;
  dsBuf.resize(dsMS);
  iSignal.read(&dsBuf[0], PredType::STD_I16LE);
  // print out first 10 and last 10 elements
  cerr << "   ";
  for(size_t i=0; i < 5; i++){
    cerr << dsBuf[i] << ", ";
  }
  cerr << "...";
  for(size_t i=dsMS-6; i < dsMS; i++){
    cerr << ", " << dsBuf[i];
  }
  cerr << endl;
  // Inspect last sequence object
  // iName = group.getObjnameByIdx(group.getNumObjs()-1);
  // Inspect third sequence object
  iName = group.getObjnameByIdx(5);
  cerr << "Last object: " << iName << endl;
  iGroup = group.openGroup(iName);
  iSignal = iGroup.openDataSet(RAWSIG_LOC);
  dsMS = iSignal.getInMemDataSize() / 2;
  cerr << " Signal Data Storage Size: " << dsMS << endl;
  dsBuf.resize(dsMS);
  iSignal.read(&dsBuf[0], PredType::STD_I16LE);
  // print out first 10 and last 10 elements
  cerr << "   ";
  for(size_t i=0; i < 5; i++){
    cerr << dsBuf[i] << ", ";
  }
  cerr << "...";
  for(size_t i=dsMS-6; i < dsMS; i++){
    cerr << ", " << dsBuf[i];
  }
  cerr << endl;
  file.close();
  // calculate median + MAD / median absolute deviation
  float dmed = median(dsBuf);
  float dmad = porebaysie::mad(dsBuf, dmed);
  std::vector<size_t> extremePoints;
  for(size_t i=0; i < dsBuf.size(); i++){
    // flag extreme deviations
    if(abs(dsBuf[i] - dmed) > (dmad * 10)){
      extremePoints.push_back(i);
    }
  }
  // extreme deviations (and their adjacent samples) are ignored
  for(auto it=extremePoints.begin(); it<extremePoints.end(); it++){
    dsBuf[*it-1] = dmed;
    dsBuf[*it] = dmed;
    dsBuf[*it+1] = dmed;
  }
  cerr << "Median: " << dmed << ", MAD: " << dmad << endl;

  // Calculate running mean, centred on the current location
  size_t windowSize = 1001;
  std::vector<unsigned short> runMean = porebaysie::runningMean(dsBuf, windowSize);

  // Calculate running min and max (of the running mean)
  size_t lastStart = 0;
  size_t lastEnd = 0;
  size_t startDist = 0;
  size_t endDist = dsBuf.size()-1; // Note: assumes size > 0
  size_t minPos = 0;
  size_t maxPos = 0;
  size_t maxWindowOffset = windowSize >> 1;
  std::vector<float> runMin;
  std::vector<float> runMax;
  std::vector<float> mxRange;
  runMin.resize(dsBuf.size());
  runMax.resize(dsBuf.size());
  mxRange.resize(dsBuf.size());
  for(size_t i=0, startDist=0, endDist=(dsBuf.size()-1);
      i < dsBuf.size(); i++, startDist++, endDist--){
    size_t windowOffset = (startDist < endDist ? startDist : endDist);
    if(windowOffset > maxWindowOffset){
      windowOffset = maxWindowOffset;
    }
    size_t currentStart = i - windowOffset;
    size_t currentEnd = i + windowOffset;
    while(lastEnd < currentEnd){
      float nextVal = runMean[++lastEnd];
      minPos = ((nextVal < runMean[minPos]) ? lastEnd : minPos);
      maxPos = ((nextVal > runMean[maxPos]) ? lastEnd : maxPos);
    }
    // recalculate min/max if needed
    if((minPos < currentStart) || (maxPos < currentStart)){
      minPos = ((minPos < currentStart) ? currentStart : minPos);
      maxPos = ((maxPos < currentStart) ? currentStart : maxPos);
      for(size_t si=currentStart; si <= currentEnd; si++){
        float nextVal = runMean[si];
        if(nextVal < runMean[minPos]){
          minPos = si;
        }
        if(nextVal > runMean[maxPos]){
          maxPos = si;
        }
      }
    }
    runMin[i] = runMean[minPos];
    runMax[i] = runMean[maxPos];
    mxRange[i] = runMax[i] - runMin[i];
  }

  float medd = median(mxRange);
  float madd = porebaysie::mad(mxRange, medd);
  cerr << "Median difference: " << medd << endl;
  cerr << "MAD difference: " << madd << endl;

  // identify difference spike points
  std::vector<size_t> changePoints;
  size_t endBuffer = 2000;
  changePoints.push_back(0);
  bool inChange = false;
  size_t lastMax = 0;
  for(size_t i=0; i < dsBuf.size(); i++){
    if(mxRange[i] > (medd + 4.5*madd)){
      if(!inChange){
        lastMax = i;
      }
      if(mxRange[i] > mxRange[lastMax]){
        lastMax = i;
      }
      inChange = true;
    } else {
      if(inChange){
        if((lastMax > endBuffer) && (lastMax < (dsBuf.size() - endBuffer))){
          size_t lastAdded = changePoints.back();
          // only add the point of largest difference when two appear
          // close to each other
          if((lastMax - lastAdded) < endBuffer){
            changePoints.pop_back();
            changePoints.push_back(
              ((mxRange[lastMax] > mxRange[lastAdded]) ?
               lastMax : lastAdded));
          } else {
            changePoints.push_back(lastMax);
          }
        }
      }
      inChange = false;
    }
  }
  changePoints.push_back(dsBuf.size());

  // determine region medians
  std::vector<float> dsAdj;
  std::vector<float> dataMedians;
  dsAdj.resize(dsBuf.size());
  for(auto itr = changePoints.begin() + 1;
      itr < changePoints.end(); itr++){
    auto itl = itr - 1;
    std::vector<uint16_t> subVec =
      std::vector<uint16_t>(dsBuf.begin() + *itl, dsBuf.begin() + *itr);
    float rangeMed = median(subVec);
    float rangeMad = porebaysie::mad(subVec, rangeMed);
    for(size_t i = *itl; i < *itr; i++){
      dsAdj[i] = (dsBuf[i] - rangeMed) / (rangeMad * 4.5);
    }
    dataMedians.push_back(rangeMed);
  }

  // Calculate local running mean to identify noisy spikes
  std::vector<float> runLocalMean = porebaysie::runningMean(dsAdj, 9);
  std::vector<float> rlmDelta = porebaysie::absDelta(runLocalMean, dsAdj);
  float rlmMad = median(rlmDelta);
  cerr << "rlmMad: " << rlmMad << endl;

  // remove local signal spikes, where peaks and dips are 1-signal transient
  // (i.e. they revert back after one signal
  size_t lastGoodValue = 0;
  bool insideSpike = false;
  size_t spikesRemoved = 0;
  for(size_t round = 0; round < 5; round++){
    for(size_t i = 1; i < (dsAdj.size()-1); i++){
      float a = dsAdj[i-1];
      float b = dsAdj[i];
      float c = dsAdj[i+1];
      float be = (a + c) / 2;
      if( ( (((b-a) < 0) && ((b-c) < 0)) ||
	    (((b-a) > 0) && ((b-c) > 0)) ) &&
	  ((abs(b - be) > (rlmMad*4.5)) ||
	   (abs(a - c) < (rlmMad*4.5))) ){
	insideSpike = true;
	spikesRemoved++;
      } else {
	if(insideSpike){
	  float replaceValue = (dsAdj[lastGoodValue] + b) / 2;
	  for(size_t si = lastGoodValue+1; si < i; si++){
	    dsAdj[si] = replaceValue;
	  }
	}
	insideSpike = false;
	lastGoodValue = i;
      }
    }
  }
  cerr << "Number of transient spikes removed after 5 rounds: "
       << spikesRemoved << endl;

  runLocalMean = porebaysie::runningMean(dsAdj, 9);

  cout << "<svg" << endl
       << "   xmlns:svg=\"http://www.w3.org/2000/svg\"" << endl
       << "   xmlns=\"http://www.w3.org/2000/svg\"" << endl
       << "   width=\"" << (dsMS / 10.0) << "mm\"" << endl
       << "   height=\"100mm\"" << endl
       << "   viewBox=\"0 0 " << (dsMS / 10.0) << " 100\"" << endl
       << "   version=\"1.1\"" << endl
       << ">" << endl;
  cout << std::fixed << setprecision(2);
  // original data
  cout << " <path fill=\"none\" stroke=\"black\" stroke-width=\"0.25\" "
       << "stroke-linejoin=\"round\" stroke-opacity=\"0.2\" d=\"M";
  for(size_t i=0; i < dsMS; i++){
    cout << " " << (i / 10.0) << "," << ((-(dsBuf[i] - dmed) /
					(dmad * 4.5)) * 50 + 50);
    if(i % 50 == 0){
      cout << endl << "   ";
    }
  }
  cout << "\" />" << endl;
  // adjusted / smoothed data
  cout << " <path fill=\"none\" stroke=\"black\" stroke-width=\"0.25\" "
       << "stroke-linejoin=\"round\" d=\"M";
  for(size_t i=0; i < dsMS; i++){
    cout << " " << (i / 10.0) << "," << (-runLocalMean[i] * 50 + 50);
    if(i % 50 == 0){
      cout << endl << "   ";
    }
  }
  cout << "\" />" << endl;
  cout << " <text x=\"5\" y=\"95\" font-size=\"6\" fill=\"black\">" << endl;
  cout << "   " << iName << endl;
  cout << " </text>" << endl;
  cout << "</svg>" << endl;
  return 0;  // successfully terminated
}
