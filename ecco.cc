/* ecco -- Electronic Characteristic Copy Observer

   Ecco is a nanopore raw-read repeat finder that identifies repeated motifs,
   and is part of the PoreBaysie nanopore toolkit.

   Copyright (C) 2019 David Eccles (gringer) <bioinformatics@gringene.org>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <unordered_map>
#include <queue>
#include <algorithm>
#include <cmath>
#include <H5Cpp.h>  // HDF5 C++ utility header file

// maximum chunk length to use for hash array
#define MAX_CHUNK_SIZE 5

using std::setprecision;
using std::cout;
using std::cerr;
using std::endl;
using std::abs;
using std::signbit;

typedef std::array<uint16_t, (MAX_CHUNK_SIZE)> kmer;

// Array hash function, using the function from boost
// [see https://stackoverflow.com/a/42701911/3389895]
// [and https://stackoverflow.com/a/50711807/3389895]
class KHasher {
public:
  std::size_t operator()(kmer const& k) const {
    std::size_t h = 0;
    for (auto e : k) {
      h ^= std::hash<uint8_t>{}(e)  + 0x9e3779b9 + (h << 6) + (h >> 2); 
    }
    return h;
  }
  static std::size_t hash(kmer const& k) {
    std::size_t h = 0;
    for (auto e : k) {
      h ^= std::hash<uint8_t>{}(e)  + 0x9e3779b9 + (h << 6) + (h >> 2); 
    }
    return h;
  }
};

using namespace H5;

// https://stackoverflow.com/a/39487448/3389895
template <typename T = float, typename C>
inline const T median(const C &v){
  if(v.empty()){
    return(0.0);
  }
  std::vector<T> tv(std::begin(v), std::end(v));
  size_t n = tv.size() / 2;
  std::nth_element(tv.begin(), tv.begin() + n, tv.end());
  if(tv.size() % 2){
    return(tv[n]);
  } else {
    // even sized vector -> average the two middle values
    auto max_it = std::max_element(tv.begin(), tv.begin() + n);
    return((*max_it + tv[n]) / 2.0);
  }
}

// Trimmed mean - (mean of values that are smaller than a given threshold)
template <typename T = float, typename C>
inline const T mean(const C &v, uint16_t threshold = 1000){
  if(v.empty()){
    return(0.0);
  }
  std::vector<T> tv(std::begin(v), std::end(v));
  float total = 0;
  size_t trimCount = 0;
  for(size_t i=0; i < v.size(); i++){
    if(v[i] < threshold){
      total += v[i];
    } else {
      trimCount++;
    }
  }
  return(total / (v.size() - trimCount));
}

template <typename T = float>
inline const float mad(std::vector<T> &v, float med){
  std::vector<float> absDevs;
  absDevs.resize(v.size());
  for(size_t i=0; i < v.size(); i++){
    absDevs[i] = abs(v[i] - med);
  }
  return(median(absDevs));
}

void usage(std::string progName){
  cerr << endl;
  cerr << "usage: " << progName << " "
       << "<multi fast5 file>" << endl << endl;
}

int main (int argc, char* argv[]){
  if(argc <= 1){
    cerr << "Error: no file specified on the command line" << endl;
    usage(argv[0]);
    return 1; // error state; unsuccessfully terminated
  }
  if(argc > 2){
    cerr << "Warning: more than one argument specified on the command line"
	 << endl;
  }
  std::unordered_map<kmer, size_t, KHasher> lastLocs;
  std::unordered_map<kmer, std::vector<size_t>, KHasher> repeatLocs;
  std::string fName = argv[1];
  const H5std_string FILE_NAME(fName);
  const H5std_string GROUP_NAME("/");
  const H5std_string RAWSIG_LOC("Raw/Signal");
  const H5std_string RAWID_LOC("Raw/channel_id");
  H5File file(FILE_NAME, H5F_ACC_RDONLY);
  // Inspect root group
  Group group = file.openGroup(GROUP_NAME);
  size_t nObjs = group.getNumObjs();
  for(size_t gPos = 0; gPos < nObjs; gPos++){
    lastLocs.clear();
    repeatLocs.clear();
    // Get name based on position, then reference by that name
    // [There is probably a direct-number way to do this]
    H5std_string iName = group.getObjnameByIdx(gPos);
    Group iGroup;
    DataSet iSignal;
    hsize_t dsMS;
    iGroup = group.openGroup(iName);
    iSignal = iGroup.openDataSet(RAWSIG_LOC);
    dsMS = iSignal.getInMemDataSize() / 2;
    cout << iName << ": ";
    //cerr << " Signal Data Storage Size: " << dsMS << endl;
    std::vector<uint16_t> dsBuf;
    dsBuf.resize(dsMS);
    iSignal.read(&dsBuf[0], PredType::STD_I16LE);
    // reduce signal resolution
    for(size_t rPos=0; rPos < dsMS; rPos++){
      dsBuf[rPos] >>= 1;
    }
    kmer ck;
    for(size_t rPos=0; rPos < dsMS - (MAX_CHUNK_SIZE); rPos++){
      std::copy(dsBuf.begin()+rPos,
		dsBuf.begin()+rPos+(MAX_CHUNK_SIZE), ck.begin());
      if(lastLocs.count(ck) == 1){
	auto val = lastLocs.find(ck);
	if(repeatLocs[ck].size() == 0){
	  repeatLocs[ck].push_back(val->second);
	}
	repeatLocs[ck].push_back(rPos);
      }
      lastLocs.emplace(ck, rPos);
    }
    cout << "(" << repeatLocs.size() << " repeated kmers)" << endl;
    for(auto rli = repeatLocs.begin(); rli != repeatLocs.end(); rli++){
      if(rli->second.size() > 0){
	ck = rli->first;
	cout << "  (";
	for(auto kit = ck.begin(); kit != ck.end(); kit++){
	  cout << " " << (*kit << 1) << "";
	}
	cout << " ) : ";
	std::vector<size_t> pv = rli->second;
	bool first = true;
	for(auto pit = pv.begin(); pit != pv.end(); pit++){
	  if(!first){
	    cout << ",";
	  } else {
	    first = false;
	  }
	  cout << *pit;
	}
	cout << endl;
      }
    }
  }
  file.close();
  return 0;  // successfully terminated
}
