run: porebaysie
	./porebaysie > out.svg
porebaysie: porebaysie.cc porebaysie.h
	clang++ -g -I/usr/include/hdf5/serial -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5_cpp -lhdf5 porebaysie.cc -o porebaysie
bouncer: bouncer.cc
	clang++ -g -I/usr/include/hdf5/serial -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5_cpp -lhdf5 bouncer.cc -o bouncer
ecco: ecco.cc
	clang++ -g -I/usr/include/hdf5/serial -L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5_cpp -lhdf5 ecco.cc -o ecco
clean:
	rm -f porebaysie out.svg
