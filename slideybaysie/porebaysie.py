#!/usr/bin/python3

## This code was created by David Eccles, with the assistance of ChatGPT
## Starting 2023-Feb-16; 2023-Feb-17

import pyarrow as pa
import sys
import toml
import flatbuffers
import struct # for parsing numbers as byte arrays

import Minknow.ReadsFormat.Footer as footer_schema
from Minknow.ReadsFormat import ContentType

## Map integer content type values to string representations
content_type_map = {
        0: "ReadsTable",
        1: "SignalTable",
        2: "ReadIdIndex",
        3: "OtherIndex"
    }

# Define a dictionary to map signal field types to PyArrow types
signal_type_map = {
        'minknow.uuid': pa.binary(16),
        'minknow.vbz': pa.large_binary(),
        'uint32': pa.uint32(),
        'int16': pa.int16()
    }

def process_schema(file_name):
    schema = toml.load(file_name)
    fields = {}
    for field_name, field_desc in schema["fields"].items():
        field_type = field_desc["type"]
        if isinstance(field_type, list):
            # if the field has multiple types, find the first type that is not an extension type
            for t in field_type:
                if t not in signal_type_map:
                    continue
                field_type = t
                break
        elif field_type not in signal_type_map:
            raise ValueError(f"Unrecognized field type: {field_type}")
        fields[field_name] = pa.field(field_name, signal_type_map[field_type], field_desc.get("nullable", True))
    return pa.schema(list(fields.values()))

pod_file_name = '../pod_multi/output.pod5'

def process_arrow_records(pod5_file, start_offset, file_length):
    pod5_file.seek(start_offset)
    # Read the Arrow magic string and continuation signature
    arrow_sig = f.read(8)
    if arrow_sig != b'ARROW1\000\000':
        raise ValueError('Invalid Arrow file header')
    # Read the continuation signature (0xFFFF)
    continuation_signature = struct.unpack('<L', f.read(4))[0]
    if continuation_signature != 0xFFFFFFFF:
        raise ValueError('Invalid continuation signature')
    # Read the metadata section
    section_data = bytearray()
    chunk_size = 0
    while continuation_signature == 0xFFFFFFFF:
        chunk_size = struct.unpack('<i', f.read(4))[0]
        print(f'Chunk size: {chunk_size}')
        if chunk_size < 0:
            break
        print(f'Initial pointer position: {hex(f.tell())}');
        section_data += f.read(chunk_size)
        print(f'Updated pointer position: {hex(f.tell())}');
        continuation_signature = struct.unpack('<L', f.read(4))[0]
    print(f"  Found an arrow record with a continuation signature; metadata length: {len(section_data)}")
    # Convert the metadata bytes to a pyarrow buffer
    with open('signal_metadata.arrow', 'wb') as f2:
        f2.write(section_data)
    metadata_buffer = pa.py_buffer(section_data)
    # Parse the metadata buffer using pyarrow
    schema = pa.ipc.read_schema(metadata_buffer)
    print(schema)
    metadata = pa.ipc.read_metadata(metadata_buffer)
    print(metadata)            

with open(pod_file_name, 'rb') as f:
    # Read the POD5 file header and first section marker
    pod_header = f.read(8)
    if pod_header != b'\x8bPOD\r\n\x1a\n':
        raise ValueError('Invalid POD5 header')
    sys.stdout.write("Found a valid POD5 file header...")
    section_marker = f.read(16)
    # Read the POD5 file footer and last section marker
    f.seek(-32, 2)
    footer_length = int.from_bytes(f.read(8), 'little', signed=True)
    sys.stdout.write(f" footer length: {footer_length}...")
    section_test = f.read(16)
    if section_test != section_marker:
        raise ValueError('Section markers don''t match (start vs end)')
    pod_footer = f.read()
    if pod_footer != pod_header:
        raise ValueError('POD5 header and footer don''t match')
    # Read the POD5 footer Table
    f.seek(-(32 + footer_length + 8), 2)
    footer_magic = f.read(8)
    if footer_magic != b'FOOTER\x00\x00':
        print(f"Footer magic: {footer_magic}")
        raise ValueError('Footer magic is incorrect')
    print(f" the POD5 shell looks good!")
    ## Load the Footer schema from the file
    footer_chunk = f.read(footer_length)
    footer_table = footer_schema.Footer.GetRootAsFooter(footer_chunk, 0)
    # Display the Footer information
    print("File Identifier:", footer_table.FileIdentifier().decode())
    print("Software:", footer_table.Software().decode())
    print("Pod5 Version:", footer_table.Pod5Version())
    print("Contents Length:", footer_table.ContentsLength())
    contents_info = []
    if not footer_table.ContentsIsNone():
        for i in range(footer_table.ContentsLength()):
            content = footer_table.Contents(i)
            content_offset = content.Offset()
            content_length = content.Length()
            content_format = content.Format()
            content_type = content.ContentType()
            print(f"  Content[{i}]: offset={content_offset}, "
                  f"length={content_length}, format={content_format}, "
                  f"type={content_type_map.get(content_type, 'Unknown')}")
            contents_info.append({
                "offset": content_offset,
                "length": content_length,
                "format": content_format,
                "type": content_type_map.get(content_type, 'Unknown')
            })
    # process embedded files
    for info in contents_info:
        f.seek(info["offset"])
        content = f.read(info["length"])
        if info["type"] == "ReadsTable":
            print("[Processing Reads Table]")
            # Process the ReadsTable content
            pass
        elif info["type"] == "SignalTable":
            print("[Processing Signal Table]")
            # Process the SignalTable content
            process_arrow_records(f, info["offset"], info["length"])
            #signal_table = pa.ipc.open_stream(buffer).read_all()
            #signal_table_root = signal_table[0].as_py()
            #signal_table_object = signal_table_root.SignalTable()
            pass
        elif info["type"] == "ReadIdIndex":
            print("[Processing Read ID Index]")
            # Process the ReadIdIndex content
            pass
        elif info["type"] == "OtherIndex":
            print("[Processing Other Index]")
            # Process the OtherIndex content
            pass
        else:
            # Handle unsupported content types
            pass
exit
