#!/usr/bin/env python3

# Created by David Eccles
# 2023-Feb-17

import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import pandas as pd
from scipy.signal import find_peaks

import pod5 as p5

from scipy.ndimage import gaussian_filter1d

# Set the filter parameters
window_size = 200
sigma = 2
peak_threshold = 5

pod_file_name = '../pod_multi/good_260bps_R10.4.1_guppy_6.4.6_LAST_gci_vs_lambda.pod5'

with p5.Reader(pod_file_name) as reader:
    for read in reader.reads():
        print(f"Found read {read.read_id}")
        print(dir(read))
        # Get the signal data and sample rate
        sample_rate = read.run_info.sample_rate
        print(f"  Read has {read.sample_count} samples with a sample rate of {sample_rate}")
        signal = read.signal
        signal_filtered = gaussian_filter1d(signal, sigma=sigma, mode='reflect')
        
        start_sample = 2000
        end_sample = start_sample + 1024

        # Compute the time steps over the sampling period
        sample_rate = read.run_info.sample_rate
        time = np.arange(start_sample, end_sample) / sample_rate

        # Slice signal and calculate smoothed version
        raw_plot = signal[start_sample:end_sample]
        smoothed_plot = signal_filtered[start_sample:end_sample]
        gradient_all = np.abs(signal_filtered)
        gradient_plot = np.abs(np.gradient(smoothed_plot))
        # Find peaks in the gradient with a minimum height of the peak threshold
        all_peaks, _ = find_peaks(gradient_all, height=peak_threshold)
        window_peaks, _ = find_peaks(gradient_plot, height=peak_threshold)
        #print(raw_plot)
        #print(smoothed_plot)
        #print(gradient)

        #Set the figure to be 3072 x 1024
        plt.rcParams["figure.figsize"] = (30.72,10.24)

        fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(30.72, 15.36))
        # Create a line plot of the raw signal in yellow, and smoothed in blue
        ax1.plot(time, raw_plot, color='salmon')
        ax1.plot(time, smoothed_plot, color='blue')
        # Plot the peaks on the gradient subplot
        ax1.plot(time[window_peaks], smoothed_plot[window_peaks], 'rx')

        ## Base Isolation
        # Find the edges of the regions
        peak_poss = np.concatenate(([0], window_peaks, [len(time)]))
        non_peak_regions = [(peak_poss[i]+2, peak_poss[i+1]-2) for i in range(len(peak_poss) - 1)]

        # Get the start and end indices of the non-steep regions
        for region in non_peak_regions:
            regMin = np.min(smoothed_plot[region[0]:(region[1]+1)])
            regMax = np.max(smoothed_plot[region[0]:(region[1]+1)])
            #ax1.axvline(x=time[region[0]], color='gray', alpha=0.2, linestyle="--")
            #ax1.axvline(x=time[region[1]], color='gray', alpha=0.2, linestyle="--")
            ax1.axvspan(xmin=time[region[0]], xmax=time[region[1]],
#                        ymin=regMin, ymax=regMax,
                        facecolor='gray', alpha=0.2)

        # Plot the gradient on the new axis
        ax2.plot(time, gradient_plot, color='green')
        # Plot the peaks on the gradient subplot
        ax2.plot(time[window_peaks], gradient_plot[window_peaks], 'rx')

        # Add a legend to differentiate the plots
        ax1.legend(['Raw Signal', 'Smoothed Signal'], loc='upper left', fontsize=20)
        ax2.legend(['Gradient'], loc='upper right', fontsize=20)
        
        # Set the X and Y axis labels
        ax1.set_xlabel('Time (s)', fontsize=20)
        ax1.set_ylabel('Signal value', fontsize=20)
        ax2.set_ylabel('Gradient', fontsize=20)
        plt.xlabel('Time (s)', fontsize=20)

        # Adjust borders
        fig.subplots_adjust(left=0.04, right=0.96)

        # Increase tick font size
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)

        # Add a title
        fig.suptitle(f"Nanopore raw signal from {read.read_id}\n[with gaussian smoothing over 200 samples, SD={sigma}]", fontsize=30)

        # Plot using matplotlib
        plt.savefig("out_image.png", dpi=100)

        # Recalculate for outside the window
        peak_poss = np.concatenate(([0], all_peaks, [len(signal)]))
        non_peak_regions = [(peak_poss[i]+2, peak_poss[i+1]-2) for i in range(len(peak_poss) - 1)]
        # Initialize lists to store the results
        region_starts = []
        region_ends = []
        transition_speeds = []
        region_speeds_smooth = []
        region_speeds_raw = []
        signals_smooth = []
        signals_raw = []
        signals_sd = []
        # Work out region gradients and instantaneous transition speed
        for start, end in non_peak_regions:
            if(end > (start + 2)):
                # Append the results to the lists
                region_starts.append(start)
                region_ends.append(end)
                # Calculate the gradient of the smoothed signal within the region
                transition_speeds.append(round(1 / ((end - start) / sample_rate), 1))
                region_speeds_smooth.append(round(np.mean(np.gradient(signal_filtered[start:end+1])), 1))
                region_speeds_raw.append(round(np.mean(np.gradient(signal[start:end+1])), 1))
                # Work out signal means
                signals_smooth.append(round(np.mean(signal_filtered[start:end+1]), 1))
                signals_raw.append(round(np.mean(signal[start:end+1]), 1))
                signals_sd.append(round(np.std(signal[start:end+1]), 1))
                
        # Create a DataFrame to store the results
        results_df = pd.DataFrame({'region_start': region_starts,
                                   'region_end': region_ends,
                                   'call_rate': transition_speeds,
                                   'speed_smooth': region_speeds_smooth,
                                   'speed_raw': region_speeds_raw,
                                   'signal_smooth': signals_smooth,
                                   'signal_raw': signals_raw,
                                   'signal_sd': signals_sd})
        # Write out to file
        results_df.to_csv('region_stats.csv')
        break
