#ifndef POREBAYSIE_H
#define POREBAYSIE_H

class porebaysie {
 public:
  template <typename T>
    static float mad(std::vector<T> &v, float med);
  template <typename T>
    static std::vector<T> runningMean(std::vector<T> &v,
                                          size_t windowSize);
  template <typename T>
    static std::vector<T> absDelta(std::vector<T> &v1,
                                       std::vector<T> &v2);
  template <typename T>
    static std::vector<T> absDelta(std::vector<T> &v1, float a2);
};

#endif
